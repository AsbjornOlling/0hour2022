extends Area2D

export var speed = 400
onready var velocity = Vector2(self.speed,0)
var sender = "nobody"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	self.translate(self.velocity.rotated(self.rotation) * delta)


func _on_Bullet_body_entered(body):
	if (not (body.name == sender)) and body.has_method("kill"):
		body.kill()
