extends KinematicBody2D

export var speed = 50
export var gravity = 2
export var rotatespeed = 2
export var velocity = Vector2(100,0)

var cooldown_default = 0.5

onready var p1 = self.name == "Player1"
onready var p2 = self.name == "Player2"
onready var cooldown = 0

onready var world = get_tree().get_root().get_node("World")

var bulletscene = preload("res://Bullet.tscn")

func _ready():
	if name == "Player1":
		$ColorRect.color = Color("#ff0000")
	elif name == "Player2": 
		$ColorRect.color = Color("#0000ff")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# rotate according to inpiut
	if (p1 and Input.is_key_pressed(KEY_DOWN)) or (p2 and Input.is_key_pressed(KEY_W)):
		self.rotation -= rotatespeed * delta
	if (p1 and Input.is_key_pressed(KEY_UP)) or (p2 and Input.is_key_pressed(KEY_S)):
		self.rotation += rotatespeed * delta
	if (p1 and Input.is_action_just_pressed("p1_shoot")) or (p2 and Input.is_action_just_pressed("p2_shoot")):
		self.shoot(name == "Player1")
	if (p1 and Input.is_action_just_pressed("p1_shootback")) or (p2 and Input.is_action_just_pressed("p2_shootback")):
		self.shoot(name == "Player2")

	# move character
	self.speed += sin(self.rotation) * gravity
	self.velocity = Vector2(speed,0).rotated(self.rotation)
	self.move_and_slide(self.velocity)
	
	# cooldown
	self.cooldown -= delta
	print(self.cooldown)

func shoot(back):
	if self.cooldown > 0:
		return
	var bullet = self.bulletscene.instance()
	bullet.rotate(self.rotation)
	if back:
		bullet.rotate(PI)
	bullet.position = self.position
	bullet.sender = self.name
	world.add_child(bullet)
	self.cooldown = self.cooldown_default

func kill():
	self.queue_free()
